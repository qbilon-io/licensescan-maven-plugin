FROM gitpod/workspace-full

USER gitpod
ENV JAVA_TOOL_OPTIONS="-Xmx4096m"

# return control
USER root
