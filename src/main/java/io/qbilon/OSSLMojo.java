package io.qbilon;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.License;
import org.apache.maven.model.building.ModelBuildingRequest;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;

@SuppressWarnings("all")
@Mojo(
    name = "audit",
    requiresDependencyCollection = ResolutionScope.COMPILE_PLUS_RUNTIME,
    requiresDependencyResolution = ResolutionScope.NONE)
public class OSSLMojo extends AbstractMojo {

  private ExecutorService exec = Executors.newSingleThreadExecutor();

  /** The Maven project. */
  @Parameter(defaultValue = "${project}", readonly = true, required = true)
  private MavenProject project;

  @Parameter(defaultValue = "${session}", readonly = true, required = true)
  private MavenSession session;

  @Component private ProjectBuilder projectBuilder;

  @Parameter(property = "printLicenses", defaultValue = "false")
  private boolean printLicenses;

  @Parameter(property = "scopes")
  private List<String> scopes;

  @Parameter(property = "allowedLicenses")
  private List<LicenseConfig> allowedLicenses;

  @Parameter(property = "manualLicenses")
  private List<ManualLicense> manualLicenses;

  @Parameter(property = "ignoredArtifacts")
  private Set<String> ignoredArtifacts;

  @Parameter(property = "ignoredSubmodules")
  private Set<String> ignoredSubmodules;

  @Parameter(property = "frontend")
  private FrontendProject frontend;

  @Parameter(property = "generateReport", defaultValue = "true")
  private boolean generateReport;

  @Parameter(property = "verbose", defaultValue = "false")
  private boolean verbose;

  // possible abortion criteria
  @Parameter(property = "failOnUnallowedLicenses", defaultValue = "false")
  private boolean failOnUnallowedLicenses;

  @Parameter(property = "failOnUnbuildableDependencies", defaultValue = "false")
  private boolean failOnUnbuildableDependencies;

  @Parameter(property = "failOnUnavailableLicenses", defaultValue = "false")
  private boolean failOnUnavailableLicenses;

  private Map<String, Set<String>> failingLicenses = new HashMap<>();
  private Map<String, LicenseReport> licenseReport = new HashMap<>();

  private final String UNBUILDABLE = "OSSL_UNBUILDABLE";
  private final String NOT_AVAILABLE = "OSSL_NOT_AVAILABLE";
  private final ObjectMapper mapper = new ObjectMapper();

  public OSSLMojo() {
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public void execute() throws MojoExecutionException, MojoFailureException {
    // Skip this project if it is in the list of ignored submodules
    if (ignoredSubmodules.contains(project.getArtifactId())) {
      info("Ignored submodule!");
      return;
    }
    if (scopes.isEmpty()) {
      scopes = List.of("compile", "provided");
    }
    if (generateReport) {
      loadData();
    }
    // check if a frontend project is configured and if the current project is the
    // frontend project
    if (frontend != null && frontend.getArtifactId() != null && frontend.getPackageJson() != null) {
      if (frontend.getArtifactId().equals(project.getArtifactId())) {
        analyzeFrontendLicenses();
      }
    }

    info("Found project: " + project);
    info(" - artifactId          : " + project.getArtifactId());
    info(" - groupId             : " + project.getGroupId());
    info(" - description         : " + project.getDescription());
    info(" - version             : " + project.getVersion());
    info(" - getArtifact.activeP : " + project.getActiveProfiles());
    info(" - getArtifact.artId   : " + project.getArtifact().getArtifactId());
    info(" - getArtifact.groupId : " + project.getArtifact().getGroupId());
    info(" - getArtifact.version : " + project.getArtifact().getVersion());
    info(" - getArtifacts.isEmpty: " + project.getArtifacts().isEmpty());

    info("BASE DEPENDENCIES");
    info("-----------------------");

    analyze(project.getDependencyArtifacts());

    info("");
    info("TRANSITIVE DEPENDENCIES");
    info("-----------------------");
    Set<Artifact> transitiveDependencies = project.getArtifacts();
    transitiveDependencies.removeAll(project.getDependencyArtifacts());

    analyze(transitiveDependencies);

    if (!failingLicenses.isEmpty()) {
      warn("FAILING LICENSES");
      warn("-----------------------");
      for (Entry<String, Set<String>> entry : failingLicenses.entrySet()) {
        Set<String> values = entry.getValue();
        switch (entry.getKey()) {
          case UNBUILDABLE:
            if (!values.isEmpty()) {
              warn("Found " + values.size() + " unbuildable dependencies:");
              for (String artifact : values) {
                warn(" - " + artifact);
              }
              if (failOnUnbuildableDependencies) {
                throw new MojoFailureException(
                    "Failing build because there are unbuildable dependencies in "
                        + project.getArtifactId());
              }
            }
            break;
          case NOT_AVAILABLE:
            if (!values.isEmpty()) {
              warn("Found " + values.size() + " artifacts with no license information:");
              for (String artifact : values) {
                warn(" - " + artifact);
              }
              if (failOnUnavailableLicenses) {
                throw new MojoFailureException(
                    "Failing build because there are artifacts in "
                        + project.getArtifactId()
                        + " with no license information attached");
              }
            }
            break;
          default:
            if (!values.isEmpty()) {
              warn(
                  "Found "
                      + values.size()
                      + " artifacts with unallowed license '"
                      + entry.getKey()
                      + "'");
              for (String artifact : values) {
                warn(" - " + artifact);
              }
              if (failOnUnallowedLicenses) {
                throw new MojoFailureException(
                    "Failing build because there are artifacts in "
                        + project.getArtifactId()
                        + " with unallowed licenses");
              }
            }
            break;
        }
      }
    }

    if (generateReport) {
      writeData();
    }
  }

  private void analyzeFrontendLicenses() throws MojoFailureException {
    File frontendProject = frontend.getPackageJson().getParentFile();
    StreamToString npmInstall = new StreamToString();
    if (executeCommand(frontendProject, "npm install", npmInstall)) {
      StreamToString licenseChecker = new StreamToString();
      if (executeCommand(
          frontendProject, "npx license-checker --production --json", licenseChecker)) {
        try {
          String json = licenseChecker.getString();
          Map<String, FrontendLicenseReport> frontendLicenses =
              mapper.readValue(json, new TypeReference<Map<String, FrontendLicenseReport>>() {});
          for (Entry<String, FrontendLicenseReport> entry : frontendLicenses.entrySet()) {
            String key = entry.getKey();
            FrontendLicenseReport value = entry.getValue();
            List<FrontendLicense> licenses = parseFrontendLicenses(value.licenses);
            for (FrontendLicense license : licenses) {
              if (!hasManualLicenseEntry(key, key)) {
                if (!isIgnoredArtifact(key)) {
                  Match allowedLicense = isAllowedLicense(license, key);
                  if (!allowedLicense.isMatch) {
                    failingLicense(license.getName(), key);
                  }
                }
              }
            }
          }
          // license
        } catch (InterruptedException | JsonProcessingException e) {
          throw new MojoFailureException(
              "Error while reading output of 'npx license-checker' due to " + e.getMessage());
        }
      } else {
        throw new MojoFailureException(
            "Error while executing 'npx license-checker' on frontend project");
      }
    } else {
      throw new MojoFailureException("Error while executing 'npm install' on frontend project");
    }
  }

  private boolean executeCommand(File directory, String command, StreamToString gobbler) {
    boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
    info(
        "Running '"
            + command
            + "' in '"
            + directory.toPath().toAbsolutePath().toString()
            + "' on OS "
            + (isWindows ? "Windows" : "Linux"),
        true);
    // we try to build a frontend dpeendency report instead of the backend one
    ProcessBuilder builder = new ProcessBuilder();
    if (isWindows) {
      builder.command("cmd.exe", "/c", command);
    } else {
      builder.command("sh", "-c", command);
    }
    builder.directory(directory);
    Process process;
    int exitCode = -1;
    try {
      process = builder.start();
      gobbler.setInputStream(process.getInputStream());
      exec.submit(gobbler);
      exitCode = process.waitFor();
    } catch (IOException e) {
      warn("IOException while executing command " + command + " due to " + e.getMessage());
    } catch (InterruptedException e) {
      warn("Execution of commane " + command + " was interrupted due to " + e.getMessage());
    }
    return exitCode == 0;
  }

  private void writeData() throws MojoFailureException {
    Path reportBase = getReportPath();
    Path reportObjPath = reportBase.resolve("report.json").toAbsolutePath();
    Path failingLicensesObjPath = reportBase.resolve("failingLicenses.json").toAbsolutePath();
    Path reportFilePath = reportBase.resolve("report.md").toAbsolutePath();
    try {
      Files.createDirectories(reportBase);
      Files.write(reportFilePath, generateReportString().getBytes());
      Files.write(reportObjPath, mapper.writeValueAsString(licenseReport).getBytes());
      Files.write(failingLicensesObjPath, mapper.writeValueAsString(failingLicenses).getBytes());
    } catch (Exception e) {
      throw new MojoFailureException(
          "Could not write report objects to "
              + reportObjPath.toString()
              + ", due to "
              + e.getMessage());
    }
  }

  private String generateReportString() throws MojoFailureException {
    StringBuilder sb = new StringBuilder();
    sb.append("# LIBRARIES BY LICENSES\n\n");
    for (Entry<String, LicenseReport> entry : licenseReport.entrySet()) {
      LicenseConfig license = entry.getValue().getLicense();
      if (license.getName() == null || license.getName().isEmpty()) {
        throw new MojoFailureException("Name of license was null or empty!");
      }
      if (license.getUrl() == null || license.getUrl().toString().isEmpty()) {
        throw new MojoFailureException(
            "URL of license " + license.getName() + " was null or empty!");
      }
      sb.append("## " + license.getName() + "\n");
      sb.append("### License URL: " + license.getUrl().toString() + "\n");
      sb.append("### Artifacts\n");
      List<String> sortedArtifacts =
          entry.getValue().getArtifacts().stream().sorted().collect(Collectors.toList());
      for (String artifact : sortedArtifacts) {
        sb.append(" * " + artifact + "\n");
      }
      sb.append("\n");
    }
    if (!failingLicenses.isEmpty()) {
      sb.append("\n\n");
      sb.append("# FAILING ARTIFACTS\n\n");
      if (failingLicenses.get(UNBUILDABLE) != null) {
        sb.append("## UNBUILDABLE ARTIFACTS\n");
        List<String> sortedArtifacts =
            failingLicenses.get(UNBUILDABLE).stream().sorted().collect(Collectors.toList());
        for (String artifact : sortedArtifacts) {
          sb.append(" * " + artifact + "\n");
        }
        sb.append("\n");
      }
      if (failingLicenses.get(NOT_AVAILABLE) != null) {
        sb.append("## ARTIFACTS WITH NO LICENSE INFORMATION\n");
        List<String> sortedArtifacts =
            failingLicenses.get(NOT_AVAILABLE).stream().sorted().collect(Collectors.toList());
        for (String artifact : sortedArtifacts) {
          sb.append(" * " + artifact + "\n");
        }
        sb.append("\n");
      }
      sb.append("## ARTIFACTS WITH UNALLOWED LICENSES\n");
      for (Entry<String, Set<String>> entry : failingLicenses.entrySet()) {
        String license = entry.getKey();
        if (!UNBUILDABLE.equals(license) && !NOT_AVAILABLE.equals(license)) {
          sb.append("### " + license + "\n");
          List<String> sortedArtifacts =
              entry.getValue().stream().sorted().collect(Collectors.toList());
          for (String artifact : sortedArtifacts) {
            sb.append(" * " + artifact + "\n");
          }
        }
      }
    }
    return sb.toString();
  }

  private void loadData() throws MojoFailureException {
    Path reportObjPath = getReportPath().resolve("report.json").toAbsolutePath();
    Path failingLicensesObjPath = getReportPath().resolve("failingLicenses.json").toAbsolutePath();
    try {
      licenseReport =
          mapper.readValue(
              reportObjPath.toFile(), new TypeReference<Map<String, LicenseReport>>() {});
    } catch (Exception e) {
      info("No existing report was found -> using empty report", true);
      licenseReport = new HashMap<>();
    }

    try {
      failingLicenses =
          mapper.readValue(
              failingLicensesObjPath.toFile(), new TypeReference<Map<String, Set<String>>>() {});
    } catch (Exception e) {
      info("No existing failingLicenses was found -> using empty failingLicenses", true);
      failingLicenses = new HashMap<>();
    }
  }

  private Path getReportPath() {
    MavenProject p = project;
    MavenProject parent = p.getParent();
    while (parent != null) {
      p = parent;
      parent = p.getParent();
    }
    return p.getFile().toPath().getParent().resolve("target").resolve("ossl");
  }

  private void analyze(Set<Artifact> dependencies)
      throws MojoExecutionException, MojoFailureException {
    ProjectBuildingRequest buildingRequest =
        new DefaultProjectBuildingRequest(session.getProjectBuildingRequest());
    buildingRequest.setValidationLevel(ModelBuildingRequest.VALIDATION_LEVEL_MINIMAL);

    for (Artifact artifact : dependencies) {
      if (scopes.contains(artifact.getScope())) {
        String artifactLabel =
            artifact.getGroupId()
                + ":"
                + artifact.getArtifactId()
                + ":"
                + artifact.getVersion()
                + ":"
                + artifact.getScope();
        String shortArtifactLabel =
            artifact.getGroupId() + ":" + artifact.getArtifactId() + ":" + artifact.getVersion();
        info(" - artifact " + artifactLabel);
        buildingRequest.setProject(null);

        // only do something if this is not handled by a manual entry
        if (!hasManualLicenseEntry(shortArtifactLabel, artifactLabel)) {
          try {
            MavenProject mavenProject =
                projectBuilder.build(artifact, buildingRequest).getProject();
            if (mavenProject.getLicenses().isEmpty()) {
              if (!isIgnoredArtifact(shortArtifactLabel)) {
                failingLicense(NOT_AVAILABLE, shortArtifactLabel);
                if (printLicenses) {
                  info("   with license: n/a");
                }
              }
            } else {
              List<License> licenses = mavenProject.getLicenses();
              for (License license : licenses) {
                if (printLicenses) {
                  info("   with license: " + license.getName());
                }
                Match allowedMatch = isAllowedLicense(license, shortArtifactLabel);
                if (!allowedMatch.isMatch) {
                  failingLicense(allowedMatch.license, shortArtifactLabel);
                  warn(
                      "WARNING: found unallowed license: "
                          + shortArtifactLabel
                          + " with "
                          + allowedMatch.license);
                }
              }
            }
          } catch (ProjectBuildingException e) {
            if (!isIgnoredArtifact(shortArtifactLabel)) {
              warn(
                  "Could not analyze license of " + artifactLabel + ". Artifact was not buildable");
              failingLicense(UNBUILDABLE, shortArtifactLabel);
            }
          }
        }
      }
    }
  }

  private boolean isIgnoredArtifact(String shortArtifactLabel) {
    if (ignoredArtifacts.contains(shortArtifactLabel)) {
      return true;
    }
    for (String ignoredArtifact : ignoredArtifacts) {
      if (ignoredArtifact.startsWith("regex:")) {
        Pattern p =
            Pattern.compile(ignoredArtifact.replace("regex:", ""), Pattern.CASE_INSENSITIVE);
        if (p.matcher(shortArtifactLabel).find()) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean hasManualLicenseEntry(String shortArtifactLabel, String artifactLabel)
      throws MojoFailureException {
    for (ManualLicense license : manualLicenses) {
      if (license.getArtifacts().contains(shortArtifactLabel)) {
        LicenseConfig config = getLicenseByName(license.getName());
        reportLicense(config, artifactLabel);
        return true;
      } else {
        for (String artifact : license.getArtifacts()) {
          if (artifact.startsWith("regex:")) {
            Pattern p = Pattern.compile(artifact.replace("regex:", ""), Pattern.CASE_INSENSITIVE);
            if (p.matcher(shortArtifactLabel).find()) {
              LicenseConfig config = getLicenseByName(license.getName());
              reportLicense(config, artifactLabel);
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  private LicenseConfig getLicenseByName(String key) throws MojoFailureException {
    for (LicenseConfig config : allowedLicenses) {
      if (config.getName().equals(key)) {
        return config;
      }
    }
    throw new MojoFailureException(
        "Manual license entry '" + key + "' does not match any of the allowed licenses!");
  }

  private void failingLicense(String license, String artifact) {
    Set<String> artifacts = failingLicenses.get(license);
    if (artifacts == null) {
      artifacts = new HashSet<>();
      failingLicenses.put(license, artifacts);
    }
    artifacts.add(artifact);
  }

  private void reportLicense(LicenseConfig license, String artifact) {
    LicenseReport report = licenseReport.get(license.getName());
    if (report == null) {
      report = new LicenseReport();
      report.setLicense(license);
      report.setArtifacts(new HashSet<>());
      licenseReport.put(license.getName(), report);
    }
    report.getArtifacts().add(artifact);
  }

  private Match isAllowedLicense(License license, String artifact) {
    String name = license.getName();
    for (LicenseConfig config : allowedLicenses) {
      if (config.getName().equals(name) || config.getAlternatives().contains(name)) {
        reportLicense(config, artifact);
        return new Match(true, config.getName());
      }
      if (config.getPattern() != null && !config.getPattern().isBlank()) {
        Pattern p = Pattern.compile(config.getPattern(), Pattern.CASE_INSENSITIVE);
        if (p.matcher(name).find()) {
          reportLicense(config, artifact);
          return new Match(true, config.getName());
        }
      }
    }
    return new Match(false, name);
  }

  private List<FrontendLicense> parseFrontendLicenses(String licenses) {
    List<FrontendLicense> result = new ArrayList<>();
    if (licenses.contains(" OR ")) {
      // multiple licenses in the form "MIT OR Apache-2.0"
      // sometimes those are in parenthesis
      if (licenses.startsWith("(")) {
        licenses = licenses.substring(1);
      }
      if (licenses.endsWith(")")) {
        licenses = licenses.substring(0, licenses.length() - 1);
      }
      String[] lics = licenses.split("OR");
      for (String license : lics) {
        result.add(new FrontendLicense(license.trim()));
      }
    } else if (licenses.contains(" AND ")) {
      // multiple licenses in the form "MIT AND BSD-3-Clause"
      // sometimes those are in parenthesis
      if (licenses.startsWith("(")) {
        licenses = licenses.substring(1);
      }
      if (licenses.endsWith(")")) {
        licenses = licenses.substring(0, licenses.length() - 1);
      }
      String[] lics = licenses.split("AND");
      for (String license : lics) {
        result.add(new FrontendLicense(license.trim()));
      }
    } else {
      // single license without ()
      result.add(new FrontendLicense(licenses.trim()));
    }
    return result;
  }

  private class Match {
    final boolean isMatch;
    final String license;

    Match(boolean isMatch, String license) {
      this.isMatch = isMatch;
      this.license = license;
    }
  }

  private void info(String msg) {
    if (verbose) {
      getLog().info(msg);
    }
  }

  private void info(String msg, boolean force) {
    if (verbose || force) {
      getLog().info(msg);
    }
  }

  private void warn(String msg) {
    getLog().warn(msg);
  }
}
