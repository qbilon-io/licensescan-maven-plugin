package io.qbilon;

import java.util.Set;

public class LicenseReport {
  private LicenseConfig license;
  private Set<String> artifacts;

  public LicenseConfig getLicense() {
    return license;
  }

  public void setLicense(LicenseConfig license) {
    this.license = license;
  }

  public Set<String> getArtifacts() {
    return artifacts;
  }

  public void setArtifacts(Set<String> artifacts) {
    this.artifacts = artifacts;
  }
}
