package io.qbilon;

import java.io.File;

public class FrontendProject {
  private String artifactId;
  private File packageJson;

  public File getPackageJson() {
    return packageJson;
  }

  public void setPackageJson(File packageJson) {
    this.packageJson = packageJson;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public void setArtifactId(String artifactId) {
    this.artifactId = artifactId;
  }
}
