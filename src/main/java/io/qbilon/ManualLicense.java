package io.qbilon;

import java.util.Set;

public class ManualLicense {
  private String name;
  private Set<String> artifacts;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getArtifacts() {
    return artifacts;
  }

  public void setArtifacts(Set<String> artifacts) {
    this.artifacts = artifacts;
  }
}
