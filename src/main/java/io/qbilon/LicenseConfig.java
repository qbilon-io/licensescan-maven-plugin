package io.qbilon;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class LicenseConfig {
  private String name = "";
  private URL url;
  private Set<String> alternatives = new HashSet<>();
  private String pattern = "";

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public URL getUrl() {
    return url;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public Set<String> getAlternatives() {
    return alternatives;
  }

  public void setAlternatives(Set<String> alternatives) {
    this.alternatives = alternatives;
  }

  public String getPattern() {
    return pattern;
  }

  public void setPattern(String pattern) {
    this.pattern = pattern;
  }
}
