package io.qbilon;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

public class StreamToString implements Runnable {
  private InputStream inputStream;
  private Consumer<String> consumer;
  private CountDownLatch latch = new CountDownLatch(1);
  private StringBuilder sb = new StringBuilder();

  public StreamToString() {
    this.consumer = s -> sb.append(s + "\n");
  }

  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void run() {
    new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
    latch.countDown();
  }

  public String getString() throws InterruptedException {
    latch.await();
    return sb.toString();
  }
}
