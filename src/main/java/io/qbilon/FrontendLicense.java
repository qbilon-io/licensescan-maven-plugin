package io.qbilon;

import org.apache.maven.model.License;

public class FrontendLicense extends License {
  private String name;

  public FrontendLicense(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }
}
