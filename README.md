# OSS License Maven Plugin

A Maven Plugin that scans the licenses of transitive dependencies of a project.
It generates a report for the whole (multi-module) project and can be configured to fail if a license is found that is unwanted.

The report is generated in the target folder of the root project under target/ossl/report.md

This plugin is based in parts on the great work of Carlo Morelli: https://github.com/carlomorelli/licensescan-maven-plugin

Additionally this plugin now can analyze frontend dependencies too. For this it needs npm to be installed on the machine that executes the scan. 
Internally it uses the npm license-checker to scan for all production licenses of a given package.json.

Once this plugin is 

## Usage
### Import
```xml
<pluginRepositories>
    <!-- Needed for the OSSL build plugin -->
    <pluginRepository>
        <id>ossl-snapshots</id>
        <url>https://gitlab.com/api/v4/projects/31177122/packages/maven</url>
    </pluginRepository>
</pluginRepositories>
```

### Configuration
```xml
<build>
    <pluginManagement>
        <plugin>
            <groupId>io.qbilon</groupId>
            <artifactId>ossl-maven-plugin</artifactId>
            <version>${ossl-maven-plugin.version}</version>
            <configuration>
                <!-- The scopes of dependencies to be included in the report -->
                <scopes>
                    <scope>compile</scope>
                    <scope>provided</scope>
                </scopes>
                <!-- Make this plugin fail strictly on all possible errors, 
                so that we get noticed as soon as there is a new or unknown dependency -->
                <!-- Fail on licenses that are not listed in the allowed licenses -->
                <failOnUnallowedLicenses>true</failOnUnallowedLicenses>
                <!-- Fail on dependencies that could not be fetched by Maven -->
                <failOnUnbuildableDependencies>true</failOnUnbuildableDependencies>
                <!-- Fail on dependencies that could be fetched but did not contain any license information -->
                <failOnUnavailableLicenses>true</failOnUnavailableLicenses>
                <!-- List of allowed licenses -->
                <allowedLicenses>
                    <license>
                        <!-- Name that should be used in the report and serves as a unique ID for this license -->
                        <name>MIT</name>
                        <!-- URL to the license text -->
                        <url>https://opensource.org/licenses/mit-license.php</url>
                        <!-- Alternative Names used in the dependencie's poms that should be treated as this license -->
                        <alternatives>
                            <alt>The MIT License</alt>
                            <alt>MIT License</alt>
                        </alternatives>
                        <!-- Regex pattern that should be used to detect alternative names for this license, 
                        can be used together with explicit alternatives -->
                        <pattern>.*MIT.*</pattern>
                    </license>
                    <license>...</license>
                </allowedLicenses>
                <!-- Artifacts that should not be included in the report, e.g., own project aritfacts -->
                <ignoredArtifacts>
                    <!-- qualified by full name -->
                    <artifact>one.test:assembly:1.0.0</artifact>
                    <!-- qualified by regex -->
                    <artifact>regex:io\.qbilon.*</artifact>
                </ignoredArtifacts>
                <!-- List of manual licenses that override automatically detected ones -->
                <manualLicenses>
                    <license>
                        <!-- Unique name used in allowedLicenses to identify the license to be used -->
                        <name>MIT</name>
                        <artifacts>
                            <!-- qualified by full name -->
                            <artifact>one.test:assembly:1.0.0</artifact>
                            <!-- qualified by regex -->
                            <artifact>regex:io\.qbilon.*</artifact>
                        </artifacts>
                    </license>
                </manualLicenses>
                <!-- Frontend project that should be scanned too via npm license-checker -->
                <frontend>
                    <artifactId>frontend</artifactId>
                    <packageJson>src/main/vue/package.json</packageJson>
                </frontend>
            </configuration>
            <executions>
                <execution>
                    <phase>compile</phase>
                    <goals>
                        <goal>audit</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </pluginManagement>
</build>
```

### Execution
Once the plugin is imported and configured correctly you can execute it via calling `mvn ossl:audit`.

A usual workflow for me when I start to audit the licenses of a larger project is to
* import/configure the plugin 
* execute the plugin and wait for it to fail because of unbuildable/not found/unallowed licenses 
* then I go throuhg the ouput and look for 
    * `[WARNING] WARNING: found unallowed license: net.java.dev.jna:jna:5.5.0 with LGPL, version 2.1`
        * For this I either add the license as a new `<license>` entry in the `<allowedLicense>` section or if it already exists I create a new `<alt>` entry
    * `[WARNING] Found 1 artifacts with no license information: io.netty:netty-tcnative-boringssl-static:2.0.43.Final`
        * For this I usually create a new `<license>` entry in the `<manualLicenses>` section or add it to an existing one
* In the end when the build succeeds this usually means, all licenses could be acquired and are allowed (they are in the allowed license section) and the report could be generated in `target/ossl/report.md` where all the dependencies are then listed by license

### Example Report

```
# LIBRARIES BY LICENSES

## MIT
### License URL: https://opensource.org/licenses/mit-license.php
### Artifacts
 * net.sf.jopt-simple:jopt-simple:4.6
 * org.projectlombok:lombok:1.18.10
 * org.slf4j:slf4j-api:1.7.30

## Apache 2.0
### License URL: https://www.apache.org/licenses/LICENSE-2.0.txt
### Artifacts
 * com.fasterxml.jackson.core:jackson-annotations:2.11.2
 * com.fasterxml.jackson.core:jackson-core:2.11.2
 * com.fasterxml.jackson.core:jackson-databind:2.11.2
 * com.fasterxml.jackson.jaxrs:jackson-jaxrs-base:2.11.2
 * com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider:2.11.2
 * com.fasterxml.jackson.module:jackson-module-jaxb-annotations:2.11.2
 * commons-codec:commons-codec:1.14
 * io.jsonwebtoken:jjwt-api:0.11.2
 * io.swagger.core.v3:swagger-annotations:2.1.1
 * io.swagger.core.v3:swagger-models:2.1.1
 * org.apache.commons:commons-math3:3.2
```

## Future Work 
* It seems additional repositories defined in the `<repositories>` section are not taken into account when trying to resolve dependencies -> should be fixed (I don't have the knowledge about maven though to do that currently)

